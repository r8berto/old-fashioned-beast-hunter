﻿using UnityEngine;
using System.Collections;

public class BigBeast : MonoBehaviour 
{
    public GameObject deadPrefab;

    private bool chasing = false;
    private Animator animator;

    private int lifes = 3;

    void Start()
    {
        this.animator = this.GetComponent<Animator>();
    }

    void Update()
    {
        if (this.chasing)
        {
            WalkLeft();
            this.animator.SetBool("Walking", true);
            return;
        }
    }

    void FixedUpdate()
    {
        RaycastHit2D hit = Physics2D.Raycast(this.transform.position + new Vector3(0, 0.6f, 0), Vector2.right * -1, 8, (1 << 8));
        if (hit.collider != null)
        {
            this.chasing = true;
        }
        else
        {
            this.chasing = false;
            this.animator.SetBool("Walking", false);
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        
        if (coll.gameObject.tag == "Bullet")
        {
            this.lifes -= 1;

            if (this.lifes <= 0)
            {
                float direction = 1;

                GameObject hunter = GameObject.Find("Hunter");

                if (hunter != null)
                {
                    if (hunter.transform.position.x > this.transform.position.x)
                    {
                        direction = -1;
                    }
                }

                Vector3 cadaverPos = new Vector3(0, 0.6f, 0);
                GameObject cadaver = Instantiate(this.deadPrefab) as GameObject;
                cadaver.transform.position = this.transform.position + cadaverPos;
                cadaver.GetComponent<Rigidbody2D>().AddForce(new Vector2(500 * direction, 100));
                cadaver.transform.localScale = new Vector3(3, 3, 3);
                Destroy(this.gameObject);
                GameObject.Find("GameLogic").GetComponent<GameLogic>().BeastDown();
            }
        }
    }

    private void WalkLeft()
    {
        float speed = -1.8f;
        this.transform.localScale = new Vector3(-1, 1, 1);
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
    }
}
