﻿using UnityEngine;
using System.Collections;

public class Bullet : MonoBehaviour 
{
    public GameObject explosionPrefab;

    private float timer = 0;
    private const float LIFE_TIME = 0.25f;

    void Update()
    {
        this.timer += Time.deltaTime;

        if (this.timer >= LIFE_TIME)
        {
            Explode(this.transform.position);
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        Explode(coll.contacts[0].point);
    }

    void Explode(Vector3 pos)
    {
        GameObject explosion = Instantiate(this.explosionPrefab, pos, Quaternion.identity) as GameObject;
        Destroy(explosion, 1);
        Destroy(this.gameObject);
    }
}
