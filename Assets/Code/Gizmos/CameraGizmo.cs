﻿using UnityEngine;
using System.Collections;

namespace CustomGizmos
{
    public class CameraGizmo : MonoBehaviour
    {
        public Vector2 size = new Vector2(16, 9);

        void OnDrawGizmos()
        {
            Gizmos.DrawWireCube(new Vector3(size.x / 2, size.y / 2, 0), new Vector3(size.x, size.y, 0));
        }
    }
}
