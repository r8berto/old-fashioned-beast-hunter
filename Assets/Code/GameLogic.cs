﻿using UnityEngine;
using System.Collections;

public class GameLogic : MonoBehaviour 
{
    public GameObject gameOverInfo;
    public GameObject huntFinishedInfo;

    private int beastsNumber = 10;
    private bool gameOver = false;

    void Update()
    {
        if (Input.GetKey(KeyCode.Escape))
        {
            Application.Quit();
        }

        if (this.gameOver)
        {
            if (Input.GetKey(KeyCode.Return))
            {
                Application.LoadLevel("Game");
            }
        }
    }

    public void GameOver()
    {
        GameObject.Find("Intro").SetActive(false);
        this.gameOverInfo.SetActive(true);
        this.gameOver = true;
    }

    public void BeastDown()
    {
        this.beastsNumber -= 1;

        if (this.beastsNumber <= 0)
        {
            HuntFinished();
        }
    }

    private void HuntFinished()
    {
        GameObject.Find("Intro").SetActive(false);
        this.huntFinishedInfo.SetActive(true);
    }
}
