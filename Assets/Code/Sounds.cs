﻿using UnityEngine;
using System.Collections;

public class Sounds : MonoBehaviour
{
    public AudioClip shoot, shootFail, reload, footstep, hurt;

    public void StopMusic()
    {
        this.gameObject.GetComponent<AudioSource>().Stop();
    }

    public void PlayShoot()
    {
        Play(this.shoot);
    }

    public void PlayShootFail()
    {
        Play(this.shootFail);
    }

    public void PlayReload()
    {
        Play(this.reload);
    }

    public void PlayFootstep()
    {
        Play(this.footstep);
    }

    public void PlayHurt()
    {
        Play(this.hurt);
    }

    private void Play(AudioClip clip, float volume = 1)
    {
        this.GetComponent<AudioSource>().PlayOneShot(clip, volume);
    }
}
