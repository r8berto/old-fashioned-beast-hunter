﻿using UnityEngine;
using System.Collections;

public class CameraControl : MonoBehaviour
{
    public GameObject followThis;

    public float smoothTime = 0.01F;
    private Vector3 velocity = Vector3.zero;

    void FixedUpdate()
    {
        if (this.followThis != null)
        {
            Vector3 newPosition = Vector3.zero;
            Vector3 targetPosition = this.followThis.transform.position;

            if (!(targetPosition.x > 50 && targetPosition.x < 64) && !(targetPosition.x > 35 && targetPosition.x < 37))
            {
                targetPosition.y += 3;
            }

            if (!this.followThis.GetComponent<Hunter>().Reloading)
            {
                float direction = this.followThis.GetComponent<Hunter>().Direction;
                targetPosition.x += direction * 2;
            }

            newPosition = Vector3.SmoothDamp(this.transform.position, targetPosition, ref velocity, smoothTime);

            if (newPosition.x < 8) newPosition.x = 8;
            if (newPosition.x > 72) newPosition.x = 72;
            if (newPosition.y > 14.5f) newPosition.y = 14.5f;
            if (newPosition.y < -3.5f) newPosition.y = -3.5f;
            newPosition.z = -10;

            this.transform.position = newPosition;
        }
    }
}
