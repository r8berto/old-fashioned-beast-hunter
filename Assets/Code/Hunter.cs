﻿using UnityEngine;
using System.Collections;

public class Hunter : MonoBehaviour
{
    public GameObject bulletPrefab;
    public GameObject casquilloPrefab;
    public GameObject ondaPrefab;
    public GameObject deadPrefab;

    public GameObject reloadingUI;
    public GameObject reloadingBar;

    public GameObject shootingUI;
    public GameObject shootingCursor;

    private GameObject sprite;
    private Animator animator;
    private Rigidbody2D rigidbody2d;

    private const float RELOADING_INCREMENT = 0.075f;
    private float direction = 1;
    private bool reloading = false;
    private float reloadingProgress = 0;

    private bool shooting = false;
    private bool shootRelease = false;
    private bool cursorIncrease = false;

    public float Direction { get { return this.direction; } }
    public bool Reloading { get { return this.reloading; } }

    private bool jumpRelease = false;
    private float footstepTimer = 0;
    private const float FOOTSTEP_RATE = 0.4f;

    void Start()
    {
        this.sprite = this.transform.Find("HunterSprite").gameObject;
        this.animator = sprite.GetComponent<Animator>();
        this.rigidbody2d = this.GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        float shoot = Input.GetAxisRaw("Shoot");
        float haxis = Input.GetAxisRaw("Horizontal");
        float jump = Input.GetAxisRaw("Jump");

        if (this.reloading)
        {
            if (haxis != 0)
            {
                float newDirection = (haxis < 0) ? -1 : 1;

                if (newDirection != this.direction)
                {
                    this.reloadingProgress += RELOADING_INCREMENT;
                    this.direction = newDirection;

                    if (this.reloadingProgress >= 1)
                    {
                        this.reloadingProgress = 0;
                        RefreshReloading(this.reloadingProgress);
                        HideReloading();
                        this.animator.SetTrigger("Reloaded");
                        this.reloading = false;
                        //GameObject.Find("Sounds").GetComponent<Sounds>().PlayReload();
                    }
                    else RefreshReloading(this.reloadingProgress);
                }
            }

            return;
        }

        if (this.shooting)
        {
            RectTransform cursor = this.shootingCursor.GetComponent<RectTransform>();

            if (cursorIncrease)
            {
                cursor.anchoredPosition = new Vector2(cursor.anchoredPosition.x + 7.5f, cursor.anchoredPosition.y);
            }
            else
            {
                cursor.anchoredPosition = new Vector2(cursor.anchoredPosition.x - 7.5f, cursor.anchoredPosition.y);
            }

            if (cursor.anchoredPosition.x <= -85 && !this.cursorIncrease)
            {
                this.cursorIncrease = true;
            }

            if (cursor.anchoredPosition.x >= 85 && this.cursorIncrease)
            {
                this.cursorIncrease = false;
            }

            if (jump != 0)
            {
                if (this.jumpRelease)
                {
                    this.shooting = false;
                    HideShooting();
                    cursor.anchoredPosition = new Vector2(85, cursor.anchoredPosition.y);
                    this.jumpRelease = false;
                }
            }
            else
            {
                this.jumpRelease = true;
            }

            if (shoot != 0)
            {
                if (this.shootRelease)
                {
                    bool fail = cursor.anchoredPosition.x > -50;
                    this.shooting = false;
                    HideShooting();
                    Shoot(fail);
                    this.reloading = true;
                    ShowReloading();
                    cursor.anchoredPosition = new Vector2(85, cursor.anchoredPosition.y);
                    if (fail)
                    {
                        GameObject.Find("Sounds").GetComponent<Sounds>().PlayShootFail();
                    }
                    else
                    {
                        GameObject.Find("Sounds").GetComponent<Sounds>().PlayShoot();
                    }
                }
            }
            else
            {
                this.shootRelease = true;
            }

            return;
        }

        if (haxis != 0) this.direction = (haxis < 0) ? -1 : 1;

        /**************************************************
         * Shooting
         *************************************************/

        if (shoot != 0 && this.rigidbody2d.velocity.y == 0)
        {
            this.shooting = true;
            this.shootRelease = false;
            ShowShooting();
            this.animator.SetBool("Walking", false);
            this.rigidbody2d.velocity = new Vector2(0, this.rigidbody2d.velocity.y);
            return;
        }

        /**************************************************
         * Jump
         *************************************************/
        if (jump != 0)
        {
            if (this.jumpRelease)
            {
                if (this.rigidbody2d.velocity.y == 0)
                {
                    this.rigidbody2d.velocity = new Vector2(this.rigidbody2d.velocity.x, 11);
                }
                this.jumpRelease = false;
            }
        }
        else
        {
            this.jumpRelease = true;
        }

        /**************************************************
         * Movement
         *************************************************/

        if (haxis == 0)
        {
            this.animator.SetBool("Walking", false);
            this.rigidbody2d.velocity = new Vector2(0, this.rigidbody2d.velocity.y);
        }
        else
        {
            this.animator.SetBool("Walking", true);
            this.sprite.transform.localScale = new Vector3(this.direction, 1, 1);
            this.rigidbody2d.velocity = new Vector2(this.direction * 4, this.rigidbody2d.velocity.y);

            this.footstepTimer += Time.deltaTime;
            if (this.footstepTimer >= FOOTSTEP_RATE)
            {
                GameObject.Find("Sounds").GetComponent<Sounds>().PlayFootstep();
                this.footstepTimer = 0;
            }
            
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Beast")
        {
            float direction = 1;

            if (coll.gameObject.transform.position.x > this.transform.position.x)
            {
                direction = -1;
            }

            GameObject.Find("Sounds").GetComponent<Sounds>().PlayHurt();
            Vector3 cadaverPos = new Vector3(0, 0.6f, 0);
            GameObject cadaver = Instantiate(this.deadPrefab) as GameObject;
            cadaver.transform.position = this.transform.position + cadaverPos;
            cadaver.GetComponent<Rigidbody2D>().AddForce(new Vector2(500 * direction, 100));
            Destroy(this.gameObject);
            GameObject.Find("GameLogic").GetComponent<GameLogic>().GameOver();
        }
    }

    private void HideReloading()
    {
        this.reloadingUI.SetActive(false);
    }

    private void ShowReloading()
    {
        this.reloadingUI.SetActive(true);
    }

    private void RefreshReloading(float progress)
    {
        this.reloadingBar.transform.localScale = new Vector3(progress, 1, 1);
    }

    private void HideShooting()
    {
        this.shootingUI.SetActive(false);
    }

    private void ShowShooting()
    {
        this.shootingUI.SetActive(true);
    }

    private void Shoot(bool fail = false)
    {
        this.animator.SetTrigger("Shoot");

        Vector3 bulletPosition = new Vector3(this.direction, 1, 0) * 0.48f;
        GameObject bullet = Instantiate(this.bulletPrefab) as GameObject;
        bullet.transform.position = this.transform.position + bulletPosition;
        bullet.transform.localScale = new Vector3(this.direction, 1, 1);
        bullet.GetComponent<Rigidbody2D>().velocity = new Vector2(25 * this.direction, 0);

        if (fail)
        {
            bullet.layer = 9;
        }

        Vector3 casquilloPosition = new Vector3(0, 0.6f, 0);
        GameObject casquillo = Instantiate(this.casquilloPrefab) as GameObject;
        casquillo.transform.position = this.transform.position + casquilloPosition;
        casquillo.GetComponent<Rigidbody2D>().AddForce(new Vector2(-this.direction * 100, 150));

        GameObject onda = Instantiate(this.ondaPrefab) as GameObject;
        onda.transform.position = this.transform.position;
        Destroy(onda, 1);
    }
}
