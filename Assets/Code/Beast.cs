﻿using UnityEngine;
using System.Collections;

public class Beast : MonoBehaviour 
{
    public float idleTime = 0f;
    public int guardTo;
    public GameObject deadPrefab;

    private bool returning = false;
    private bool alerted = false;
    private bool chasing = false;
    private float chaseX = 0;
    private bool idle = true;
    private float idleTimer = 0;
    private Vector3 originalPosition;

    private Animator animator;

    void Start()
    {
        this.originalPosition = this.transform.position;
        this.animator = this.GetComponent<Animator>();
    }

    void Update()
    {
        if (this.chasing)
        {
            float targetX = this.chaseX;
            float diference = targetX - this.transform.position.x;

            if (diference > 0.5f) WalkRight();
            else if (diference < -0.5f) WalkLeft();
            else
            {
                this.chasing = false;
                this.alerted = false;
                this.returning = true;
                this.idle = false;
                this.animator.SetBool("Walking", true);
            }

            return;
        }

        if (this.idle)
        {
            if (this.alerted)
            {
                this.idle = false;
                this.animator.SetBool("Walking", true);
            }
            else
            {
                this.idleTimer += Time.deltaTime;

                if (this.idleTimer >= this.idleTime)
                {
                    this.idleTimer = 0;
                    this.idle = false;
                    this.animator.SetBool("Walking", true);
                }
            }
            return;
        }

        if (this.returning)
        {
            float targetX = this.originalPosition.x;
            float diference = targetX - this.transform.position.x;

            if      (diference > 0.1f)  WalkRight();
            else if (diference < -0.1f) WalkLeft();
            else
            {
                this.returning = false;
                this.idle = true;
                this.animator.SetBool("Walking", false);
            }
        }
        else
        {
            float targetX = this.originalPosition.x + this.guardTo;
            float diference = targetX - this.transform.position.x;

            if      (diference > 0.1f)  WalkRight();
            else if (diference < -0.1f) WalkLeft();
            else
            {
                this.returning = true;
                this.idle = true;
                this.animator.SetBool("Walking", false);
            }
        }
    }

    void FixedUpdate()
    {
        float direction = this.transform.localScale.x;
        float distance = (this.alerted) ? 8 : 4;

        RaycastHit2D hit = Physics2D.Raycast(this.transform.position + new Vector3(0, 0.6f, 0), Vector2.right * direction, distance, (1 << 8));
        if (hit.collider != null)
        {
            this.chasing = true;
            this.alerted = true;
            this.chaseX = hit.collider.gameObject.transform.position.x;
        }
        else
        {
            if (this.chasing)
            {
                if (this.chaseX > this.transform.position.x)
                {
                    this.chaseX -= 1;
                }
                else if (this.chaseX < this.transform.position.x)
                {
                    this.chaseX += 1;
                }
            }
        }
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Bullet")
        {
            float direction = 1;

            GameObject hunter = GameObject.Find("Hunter");

            if (hunter != null)
            {
                if (hunter.transform.position.x > this.transform.position.x)
                {
                    direction = -1;
                }
            }

            Vector3 cadaverPos = new Vector3(0, 0.6f, 0);
            GameObject cadaver = Instantiate(this.deadPrefab) as GameObject;
            cadaver.transform.position = this.transform.position + cadaverPos;
            cadaver.GetComponent<Rigidbody2D>().AddForce(new Vector2(500 * direction, 100));
            Destroy(this.gameObject);
            GameObject.Find("GameLogic").GetComponent<GameLogic>().BeastDown();
        }
    }

    void OnTriggerEnter2D(Collider2D coll)
    {
        if (coll.gameObject.tag == "Onda")
        {
            this.alerted = true;
        }
    }

    private void WalkRight()
    {
        float speed = (this.alerted) ? 6 : 2;
        this.transform.localScale = new Vector3(1, 1, 1);
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
    }

    private void WalkLeft()
    {
        float speed = (this.alerted) ? -6 : -2;
        this.transform.localScale = new Vector3(-1, 1, 1);
        this.GetComponent<Rigidbody2D>().velocity = new Vector2(speed, 0);
    }
}
